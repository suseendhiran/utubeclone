import React, { useState, useEffect } from "react";

import { Grid } from "@material-ui/core";
import SearchBar from "./components/SearchBar/SearchBar";
import VideoDetail from "./components/VideoDetail/VideoDetail";
import youtube from "./api/youtube";
import VideoList from "./components/VideoList/VideoList";

function App() {
  const [videos, setVideos] = useState([]);
  const [selectedVideo, setSelectedVideo] = useState(null);
  useEffect(() => {
    const searchValue = localStorage.getItem("video");
    console.log(searchValue);
    handleSearch(searchValue);
  }, []);

  const handleSearch = async (searchValue) => {
    const response = await youtube.get("search", {
      params: {
        part: "snippet",
        maxResults: 4,
        key: "AIzaSyCO4DvxEfmYbdrwi2gcY-1CRrKebqiAHig",
        q: searchValue,
      },
    });
    console.log(response);
    setVideos(response.data.items);
    setSelectedVideo(response.data.items[0]);
    localStorage.setItem("video", JSON.stringify(searchValue));
  };
  const handleSelectedVideo = (video) => {
    setSelectedVideo(video);
  };
  return (
    <Grid justify="center" container spacing={4} style={{ width: "100vw" }}>
      <Grid item xs={12}>
        <Grid container spacing={10}>
          <Grid item xs={12}>
            <SearchBar handleSearch={handleSearch} />
          </Grid>
          <Grid item xs={7}>
            <VideoDetail video={selectedVideo} />
          </Grid>
          <Grid item xs={5}>
            <VideoList videos={videos} onSelectVideo={handleSelectedVideo} />
          </Grid>
        </Grid>
      </Grid>
    </Grid>
  );
}

export default App;
