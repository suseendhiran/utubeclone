import React, { useState } from "react";
import { Paper, TextField } from "@material-ui/core";

import styles from "./SearchBar.module.css";

function SearchBar(props) {
  const [searchValue, setSearchValue] = useState("");

  const handleChange = (event) => setSearchValue(event.target.value);
  const handleSubmit = (event) => {
    event.preventDefault();
    props.handleSearch(searchValue);
  };
  return (
    <Paper elevation={6} className={styles.paper}>
      <form onSubmit={handleSubmit}>
        <TextField
          fullWidth
          label="Search...."
          onChange={handleChange}
        ></TextField>
      </form>
    </Paper>
  );
}

export default SearchBar;
