import React from "react";
import { Grid, Paper, Typography } from "@material-ui/core";

import styles from "./VideoItem.module.css";

function VideoItem({ video, onSelectVideo }) {
  return (
    <Grid item xs={12}>
      <Paper className={styles.video} onClick={() => onSelectVideo(video)}>
        <img
          className={styles.image}
          alt="sample"
          src={video.snippet.thumbnails.medium.url}
        />
        <Typography variant="subtitle1">
          <b>{video.snippet.title}</b>
        </Typography>
      </Paper>
    </Grid>
  );
}

export default VideoItem;
