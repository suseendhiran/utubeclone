import React from "react";
import { Grid } from "@material-ui/core";

import VideoItem from "../VideoItem/VideoItem";
function VideoList({ videos, onSelectVideo }) {
  const videosList = videos.map((video, index) => (
    <VideoItem key={index} video={video} onSelectVideo={onSelectVideo} />
  ));
  return (
    <Grid container spacing={6}>
      {videosList}
    </Grid>
  );
}

export default VideoList;
